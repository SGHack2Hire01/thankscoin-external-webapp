<!doctype html>
<html>
<head>
	<title>QR Donate</title>
	<meta charset='utf-8'>
    <script src="jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="grid.js"></script>
	<script type="text/javascript" src="version.js"></script>
	<script type="text/javascript" src="detector.js"></script>
	<script type="text/javascript" src="formatinf.js"></script>
	<script type="text/javascript" src="errorlevel.js"></script>
	<script type="text/javascript" src="bitmat.js"></script>
	<script type="text/javascript" src="datablock.js"></script>
	<script type="text/javascript" src="bmparser.js"></script>
	<script type="text/javascript" src="datamask.js"></script>
	<script type="text/javascript" src="rsdecoder.js"></script>
	<script type="text/javascript" src="gf256poly.js"></script>
	<script type="text/javascript" src="gf256.js"></script>
	<script type="text/javascript" src="decoder.js"></script>
	<script type="text/javascript" src="qrcode.js"></script>
	<script type="text/javascript" src="findpat.js"></script>
	<script type="text/javascript" src="alignpat.js"></script>
	<script type="text/javascript" src="databr.js"></script>

    <script type="text/javascript" src="checkbalance.js"></script>
    <script type="text/javascript" src="qrmain.js"></script>

    <script type="text/javascript">
        window.poolaccount = '<?=$POOLACC?>';
        window.updateAccountBalance = function(balance) {
            $('#accbalance').html(balance);
        }
    </script>

    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no" />
    <link href="style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="main.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>
<div class="wrap">
    <header>
        <div class="logo">
            <a href="#">
                <div class="camera">
                    <video id="video">Video stream not available.</video>
                </div>
                <canvas id="qr-canvas">
                </canvas>
                <span class="title"><span>Donate</span>.Coin</span>
            </a>
            <p>Donate from the common pool of <span id="accbalance">...</span> coins. Scan the QR code on the presentation.</p>
        </div>
    </header>
</body>



</html>

<?php
    $amtlist = [1,2,5,10,20,50];
?>
<!doctype html>
<html>
<head>
	<title>QR Donate</title>
	<meta charset='utf-8'>
    <script src="jquery-3.2.0.min.js"></script>
    <script src="checkbalance.js"></script>

    <script type="text/javascript">
        window.poolaccount = '<?=$POOLACC?>';
        window.updateAccountBalance = function(balance) {
            $('#accbalance').html(balance);
            <?php foreach ($amtlist as $amt) { ?>
                if (balance < <?=$amt?>) $('#amt_<?=$amt?>').hide();
            <?php } ?>
        }
    </script>

    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no" />
    <link href="style.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>
<div class="wrap">
    <header>
        <div class="logo">
            <a href="#">
                <img src="images/logo.png" alt="logo by mobifreaks"/>
                <span class="title"><span>Donate</span>.Coin</span>
            </a>
            <p>You are donating from the common pool of <span id="accbalance">...</span> coins to account <b><?=$_GET['to']?></b>. Please select amount.</p>
        </div>
        <nav class="vertical menu">
            <ul>
                <?php foreach ($amtlist as $amt) { ?>
                <li id="amt_<?=$amt?>"><a href="/donate/?to=<?=$_GET['to']?>&amount=<?=$amt?>">Donate <?=$amt?> coins</a></li>
                <?php } ?>
           </ul>
        </nav>
    </header>
</body>
</html>

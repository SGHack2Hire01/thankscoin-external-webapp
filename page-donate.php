<?php

$xfer = [
    "accountFrom" => $POOLACC,
    "accountTo" => $_GET['to'],
    "amount" => $_GET['amount'],
    "description" => 'donation',
    "signature" => "OK"
];

$data_string = json_encode($xfer);
$ch = curl_init('http://h.r2.io/ledger/transaction');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);

if ($result === FALSE) {
    $error = true;
}
else {
    $error = false;
}

?>
<!doctype html>
<html>
<head>
	<title>QR Donate</title>
	<meta charset='utf-8'>
    <script src="jquery-3.2.0.min.js"></script>
    <script src="checkbalance.js"></script>

    <script type="text/javascript">
        window.poolaccount = '<?=$POOLACC?>';
        window.updateAccountBalance = function(balance) {
            $('#accbalance').html(balance);
        }
    </script>

    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no" />
    <link href="style.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>
<div class="wrap">
    <header>
        <div class="logo">
            <a href="#">
                <img src="images/logo.png" alt="logo by mobifreaks"/>
                <span class="title"><span>Donate</span>.Coin</span>
            </a>
            <p>Thank you for your kind donation of <?=$_GET['amount']?> coins. The common pool has <span id="accbalance">...</span> coins left.</p>
            <?php if ($error) { ?>
                <p>Unfortunately your donation could not be processed.</p>
            <?php } ?>
        </div>
    </header>
</body>
</html>

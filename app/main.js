/**
 * Main application module
 */
define(['knockout', 'koSwitch'], function(ko) {

    function AppViewModel(params) {

        $.ajaxSetup({
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });

    }

    // single instance
    return { instance: new AppViewModel()};

});
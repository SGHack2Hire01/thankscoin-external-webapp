// load all application dependencies and initialize knockout application
define([
    'knockout', 'koMapping', 'koGrid', 'koSwitch',
    'jquery', 'jquery-ui', 'datatables.bootstrap',
    'modernizr', 'bootstrap', 'simple-slider'
], function (ko, koMapping) {

    // register koMapping plugin globally
    ko.mapping = koMapping;

    ko.components.register('main-app', {
        viewModel: { require: 'app/main'},
        template: { require: 'text!app/main.html' }
    });

    ko.components.register('account-totals', {
        viewModel: { require: 'app/account-totals'},
        template: { require: 'text!app/account-totals.html' }
    });

    ko.components.register('account-info', {
        viewModel: { require: 'app/account-info'},
        template: { require: 'text!app/account-info.html' }
    });

    ko.components.register('transaction-totals', {
        viewModel: { require: 'app/transaction-totals'},
        template: { require: 'text!app/transaction-totals.html' }
    });

    ko.components.register('transaction-info', {
        viewModel: { require: 'app/transaction-info'},
        template: { require: 'text!app/transaction-info.html' }
    });

    ko.components.register('account-log-summary', {
        viewModel: { require: 'app/account-log-summary'},
        template: { require: 'text!app/account-log-summary.html' }
    });


    // datatables binding by zachpainter77
    // https://datatables.net/forums/discussion/31797/knockout-js-3-4-custom-binding-for-jquery-datatables-net

    ko.bindingHandlers.dataTablesForEach = {
        page: 0,
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = ko.unwrap(valueAccessor());
            ko.unwrap(options.data);
            if(options.dataTableOptions.paging){
                valueAccessor().data.subscribe(function (changes) {
                    var table = $(element).closest('table').DataTable();
                    ko.bindingHandlers.dataTablesForEach.page = table.page();
                    table.destroy();
                }, null, 'arrayChange');
            }
            var nodes = Array.prototype.slice.call(element.childNodes, 0);
            ko.utils.arrayForEach(nodes, function (node) {
                if (node && node.nodeType !== 1) {
                    node.parentNode.removeChild(node);
                }
            });
            return ko.bindingHandlers.foreach.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var options = ko.unwrap(valueAccessor()),
                key = 'DataTablesForEach_Initialized';
            ko.unwrap(options.data);
            var table;
            if(!options.dataTableOptions.paging){
                table = $(element).closest('table').DataTable();
                table.destroy();
            }
            ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);
            table = $(element).closest('table').DataTable(options.dataTableOptions);
            if (options.dataTableOptions.paging) {
                if (table.page.info().pages - ko.bindingHandlers.dataTablesForEach.page == 0)
                    table.page(--ko.bindingHandlers.dataTablesForEach.page).draw(false);
                else
                    table.page(ko.bindingHandlers.dataTablesForEach.page).draw(false);
            }
            if (!ko.utils.domData.get(element, key) && (options.data || options.length))
                ko.utils.domData.set(element, key, true);
            return { controlsDescendantBindings: true };
        }
    };

    // end of datatables binding

    // numericText by Stijn
    // http://stackoverflow.com/questions/7704268/formatting-rules-for-numbers-in-knockoutjs
    ko.bindingHandlers.numericText = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var positions= ko.utils.unwrapObservable(allBindingsAccessor().positions) || ko.bindingHandlers.numericText.defaultPositions;
            var formattedValue = value.toFixed(positions);
            var finalFormatted = ko.bindingHandlers.numericText.withCommas(formattedValue);

            ko.bindingHandlers.text.update(element, function() { return finalFormatted ; });
        },

        defaultPositions: 2,

        withCommas: function(original){
            original+= '';
            x = original.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;

        }
    };

    // the main application is started with an empty viewmodel, the main component defines the real view model
    function EmptyViewModel() {}
    ko.applyBindings(new EmptyViewModel())

});


/**
 * Application models (Account, Transaction)
 */
define(['knockout'], function(ko) {

    /**
     * Model constructed from account object
     */
    function Account(data) {
        var _this = this;

        ko.mapping.fromJS(data, {}, this);

    };

    /**
     * Model constructed from account summary statistics object
     */
    function AccountSummary(data, sec) {
        var _this = this;

        ko.mapping.fromJS(data, {}, this);
        this.count(this.count()/sec);
        this.volume(this.volume()/sec);
    };


    /**
     * Model constructed from transaction object
     */
    function Transaction(data) {
        var _this = this;

        ko.mapping.fromJS(data, {}, this);

    };

    return {
        Account: Account,
        AccountSummary: AccountSummary,
        Transaction: Transaction
    }


});

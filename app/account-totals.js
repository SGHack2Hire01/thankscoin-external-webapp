/*
 * Account list parsed and totals / display lists calculated
 */
define(['knockout', 'app/model', 'jquery', 'jquery-ui'], function(ko, model, $) {

    function AccountTotalsModel(params) {
        var _this = this;

        _this.apiUrl = '//h.r2.io/ledger/accounts';
        _this.topApiUrl = '//h.r2.io/ledger/topaccounts';
        _this.totalApiUrl = '//h.r2.io/ledger/totalbalance';
        _this.TOP_COUNT = 3;

        _this.topAccounts = ko.observableArray();
        _this.recentlyCreated = ko.observableArray();
        _this.recentlyActive = ko.observableArray();
        _this.totalBalance = ko.observable(0);

        // this is not used anymore
        _this.refreshList = function() {
            $.ajax({
                type: 'GET',
                url: _this.apiUrl,
                error: function(xhr, status, response) {
                    console.log('error retrieving account list');
                    _this.error("Server error while retrieving account list, retrying soon.");
                },
                success: function(data, status, xhr) {
                    console.log('account list retrieved');
                    // total balance
                    var total = 0;
                    $.each(data, function(i, d) {
                        total += d['balance'];
                    });
                    _this.totalBalance(total);

                    // recently created
                    var created = data.sort(function(a,b) { return b['created']-a['created']} ).slice(0, _this.TOP_COUNT);
                    _this.recentlyCreated( $.map(created, function(d) { return new model.Account(d); }) );

                    // recently active
                    var lasttx = data.sort(function(a,b) { return b['last_transaction']-a['last_transaction']} ).slice(0, _this.TOP_COUNT);
                    _this.recentlyActive( $.map(lasttx, function(d) { return new model.Account(d); }) );

                    // top accounts
                    var top = data.sort(function(a,b) { return b['balance']-a['balance']} ).slice(0, _this.TOP_COUNT);
                    _this.topAccounts( $.map(top, function(d) { return new model.Account(d); }) );
                }
            });
        };

        _this.refreshTop = function() {
            $.ajax({
                type: 'GET',
                url: _this.topApiUrl,
                data: {
                    number: _this.TOP_COUNT + 1 // to remove corporate :)
                },
                error: function(xhr, status, response) {
                    console.log('error retrieving account list');
                    _this.error("Server error while retrieving account list, retrying soon.");
                },
                success: function(data, status, xhr) {
                    console.log('account list retrieved');

                    data.shift(); // remove first one - should be corporate :))))))))
                    _this.topAccounts( $.map(data, function(d) { return new model.Account(d); }) );
                }
            });
        };

        _this.refreshTotal = function() {
            $.ajax({
                type: 'GET',
                url: _this.totalApiUrl,
                error: function(xhr, status, response) {
                    console.log('error retrieving account totals');
                    _this.error("Server error while retrieving total balance, retrying soon.");
                },
                success: function(data, status, xhr) {
                    console.log('account list retrieved');

                    _this.totalBalance(data['totalBalance']);
                }
            });
        };


        _this.error = ko.observable(null);

        window.setInterval(function() {
            _this.refreshTop()
        }, 1000);

        window.setInterval(function() {
            _this.refreshTotal()
        }, 1000);


        _this.refreshTop();
        _this.refreshTotal();
    }

    return { viewModel: AccountTotalsModel};

});

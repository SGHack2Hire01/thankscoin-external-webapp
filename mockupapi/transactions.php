<?php

header('Content-type: application/json');

$in = json_decode(file_get_contents('/tmp/transactions.json'), true);

$out = [];
foreach ($in as $tx) {
    if (isset($_GET['afterId']) && $tx['transaction_id'] <= $_GET['afterId']) continue;
    if (isset($_GET['txTimeFrom']) && $tx['timestamp'] < $_GET['txTimeFrom']) continue;
    $out[] = $tx;
}


echo json_encode($out);

/*
function randomtx() {
	return [
		"transaction_id" => rand(1, 1000),
		"type" => 'transfer',
		"from" => md5(rand()),
		"to" => md5(rand()),
		"amount" => rand(1, 1000),
		"description" => "xxx",
		"timestamp" => date("c"),
		"signature" => "OK"
	];
}

$cnt = rand(1, 5);
$ret = [];
while ($cnt-- > 0) $ret[] = randomtx();

header('Content-type: application/json');
echo json_encode($ret);

*/

<?php
/*
 * Generate mock transaction data
 *
 * Accounts have a probability for sending and receiving
 *
 * Amounts also distributed probabilistically
 */

$desclist = file('../mockupapi/nounlist.txt');

$PROB_REC = [
  [
      "prob" => 0.01,
      "recvprob" => 1.8, // fake probability density for receiving money
      "sendprob" => 0.005, // probability of sending money at any given time
      "active" => true
  ],
  [
      "prob" => 0.05,
      "recvprob" => 0.02,
      "sendprob" => 0.02,
      "active" => true
  ],
  [
      "prob" => 0.05,
      "recvprob" => 0.00,
      "sendprob" => 0.015,
      "active" => true
  ],
  [
      "prob" => 0.9,
      "active" => false
  ]
];

$accounts = json_decode(file_get_contents("fakeaccounts.json"));
$ACC_CNT = count($accounts);

$accprob = [];
$rscale = 0;

$nextInitProb = 0;


function random() {
    return mt_rand() / mt_getrandmax();
}

function reinitProb() {
    global $accounts, $accprob, $PROB_REC, $rscale, $nextInitProb;

    echo "Reinitializing probabilities patterns\n";
    $accprob = [];
    $rscale = 0;
    foreach ($accounts as $number) {
        $p = random();
        $j = 0;
        while ($p > $PROB_REC[$j]['prob']) {
            $p -= $PROB_REC[$j]['prob'];
            $j++;
        }
        $r = $PROB_REC[$j];
        if (!$r['active']) continue;
        $accprob[$number] = $r;
        $rscale += $r['recvprob'];
    }
    $nextInitProb = rand(20, 60);
}

reinitProb();
while (true) {
    $now = date('c');
    echo "Simulating transactions @ $now\n";

    foreach ($accprob as $from=>$prob) {
        if (random() >= $prob['sendprob']) continue;
        // select to account to send to
        $p = random() * $rscale;
        $to = false;
        foreach($accprob as $accid => $ap) {
            if ($p < $ap['recvprob'] && $accid != $from) {
                $to = $accid;
                break;
            }
            else $p -= $ap['recvprob'];
        }
        if (!$to) continue; // should not happen
        $amt = rand(1, 50);
        if ($amt > 45) $amt += 20 * ($amt-45);
        else if ($amt > 40) $amt += 10 * ($amt - 40);

        $desc = trim($desclist[mt_rand(0, count($desclist) - 1)]);

        $xfer = [
            "accountFrom" => $from,
            "accountTo" => $to,
            "amount" => $amt,
            "description" => $desc,
            "signature" => "OK"
        ];

        $data_string = json_encode($xfer);
        $ch = curl_init('http://h.r2.io/ledger/transaction');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        echo "-- {$from} => {$to} - $amt\n";

        if ($result === FALSE) {
            echo "err: {curl_errno($ch)}\n";
            continue;
        }
        else {
            echo "$result\n";
        }

    }

    sleep(1);

    if ($nextInitProb-- < 0) reinitProb();
}
